# EKG processing

Implementation of the project on Digital processing
of signals and images.

## One time set-up

```bash
git clone https://gitlab.com/Sztosik/ekg-processing.git
cd ekg-processing
poetry install --no-root
```

## Run project

```bash
poetry shell
streamlit run ekg_processing/__main__.py
```
