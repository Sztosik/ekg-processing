import pandas as pd
import streamlit as st

DATA_FREQUENCY_HZ = 100.0


st.title("EKG Processing")
st.subheader("ekg1.csv")

# # Load signal
# df = pd.read_csv("./signals/ekg1.csv")
# samples_count = len(df)
# df.insert(0, "time", [t / DATA_FREQUENCY_HZ for t in range(1, samples_count + 1)])

# # Signals selection
# options = st.multiselect("Sellect signals", [col for col in df.columns][1:], ["s1"])

# # Displaying line chart
# st.line_chart(df, x="time", y=options)


st.subheader("ekg100.csv")

df100 = pd.read_csv("./signals/ekg100.csv")
samples_count100 = len(df100)
print(samples_count100)
df100.insert(0, "time", [t / 360.0 for t in range(1, samples_count100 + 1)])
st.line_chart(df100, x="time", y="y")
