# 1. Wygeneruj ciąg próbek odpowiadający fali sinusoidalnej o częstotliwości 50 Hz
# i długości
#
# 2. wyznacz dyskretną transformatę Fouriera tego sygnału i przedstaw jego widmo
# amplitudowe na wykresie w zakresie częstotliwości [0, Fs/2], gdzie Fs oznacza
# częstotliwość próbkowania
#
# 3. Wygeneruj ciąg próbek mieszaniny dwóch fal sinusoidalnych (tzn. ich kombinacji
# liniowej) o częstotliwościach 50 i 60 Hz. Wykonaj zadanie z punktu 2 dla tego
# sygnału.

import matplotlib.pyplot as plt
import numpy as np

SIN_WAVE_FREQ_HZ = 50
SAMPLING_FREQ_HZ = 6_000.0
TOTAL_SAMPLES = 65_536

f0 = SIN_WAVE_FREQ_HZ
f1 = 60.0              # Wave frequency [Hz]
Fs = SAMPLING_FREQ_HZ  # Sampling frequency [Hz]
n = TOTAL_SAMPLES      # Total number of samples

t_step = 1 / Fs  # Sample time interval
f_step = Fs / n  # Frequency interval

t = np.linspace(0, (n - 1) * t_step, n)  # Time steps
f = np.linspace(0, (n - 1) * f_step, n)  # Frequency steps

# 1. Generowanie sygnału sinusoidalnego o częstotliwości 50 Hz
y1 = 2 * np.sin(2 * np.pi * f0 * t)  # Sinusoidal wave with frequency f0

# 2. Wyznaczanie dyskretnej transformaty Fouriera i widma amplitudowego dla sygnału y1
fft_y1 = np.fft.fft(y1)
mag_y1 = np.abs(fft_y1) / n
mag_y1 = mag_y1[: n // 2 + 1]
mag_y1[1:] = 2 * mag_y1[1:]  # Skalowanie amplitudy
f_plot_y1 = f[: n // 2 + 1]

# 3. Generowanie sygnału sumy dwóch fal sinusoidalnych o częstotliwościach 50 i 60 Hz
y2 = np.sin(2 * np.pi * f1 * t)  # Sinusoidal wave with frequency f1
y = y1 + y2

# Wyznaczanie dyskretnej transformaty Fouriera i widmo amplitudowe dla sygnału y
fft_y = np.fft.fft(y)
mag_y = np.abs(fft_y) / n
mag_y = mag_y[: n // 2 + 1]
mag_y[1:] = 2 * mag_y[1:]  # Skalowanie amplitudy
f_plot_y = f[: n // 2 + 1]

# Wyznaczanie odwrotnej transformaty Fouriera
ifft_y1 = np.fft.ifft(fft_y1)
ifft_y = np.fft.ifft(fft_y)

# Wykresy
fig, axs = plt.subplots(2, 3, figsize=(18, 10))

# Wykres sygnału y1
axs[0, 0].plot(t, y1)
axs[0, 0].set_title("Sygnał sinusoidalny o częstotliwości 50 Hz")
axs[0, 0].set_xlabel("Czas [s]")
axs[0, 0].set_ylabel("Amplituda")

# Wykres widma amplitudowego sygnału y1
axs[0, 1].plot(f_plot_y1, mag_y1)
axs[0, 1].set_title("Widmo amplitudowe sygnału 50 Hz")
axs[0, 1].set_xlabel("Częstotliwość [Hz]")
axs[0, 1].set_ylabel("Amplituda")

# Wykres odwrotnej transformaty Fouriera dla y1
axs[0, 2].plot(t, ifft_y1.real)
axs[0, 2].set_title("Odwrotna transformata Fouriera dla sygnału 50 Hz")
axs[0, 2].set_xlabel("Czas [s]")
axs[0, 2].set_ylabel("Amplituda")

# Wykres sygnału y
axs[1, 0].plot(t, y)
axs[1, 0].set_title("Sygnał sumy dwóch fal sinusoidalnych (50 Hz i 60 Hz)")
axs[1, 0].set_xlabel("Czas [s]")
axs[1, 0].set_ylabel("Amplituda")

# Wykres widma amplitudowego sygnału y
axs[1, 1].plot(f_plot_y, mag_y)
axs[1, 1].set_title("Widmo amplitudowe sygnału sumy (50 Hz i 60 Hz)")
axs[1, 1].set_xlabel("Częstotliwość [Hz]")
axs[1, 1].set_ylabel("Amplituda")

# Wykres odwrotnej transformaty Fouriera dla y
axs[1, 2].plot(t, ifft_y.real)
axs[1, 2].set_title(
    "Odwrotna transformata Fouriera dla sygnału sumy (50 Hz i 60 Hz)"
)
axs[1, 2].set_xlabel("Czas [s]")
axs[1, 2].set_ylabel("Amplituda")

plt.tight_layout()
plt.show()
