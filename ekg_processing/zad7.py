import streamlit as st
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import cv2
import math
from streamlit_cropper import st_cropper

def load_image(image_file):
    return Image.open(image_file)

def display_image(image, title=""):
    st.image(image, caption=title, use_column_width=True)

def plot_grayscale_levels(image, line_coord, orientation='horizontal'):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)

    plt.figure()
    if orientation == 'horizontal':
        levels = grayscale_array[line_coord, :]
        plt.plot(levels)
        plt.title(f'Grayscale levels along horizontal line at y={line_coord}')
    elif orientation == 'vertical':
        levels = grayscale_array[:, line_coord]
        plt.plot(levels)
        plt.title(f'Grayscale levels along vertical line at x={line_coord}')

    plt.xlabel('Pixel position')
    plt.ylabel('Grayscale level')
    st.pyplot(plt)

def plot_histogram(image, title="Histogram"):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)
    
    plt.figure()
    plt.hist(grayscale_array.flatten(), bins=256, range=[0, 256], color='black', alpha=0.7)
    plt.title(title)
    plt.xlabel('Pixel intensity')
    plt.ylabel('Frequency')
    st.pyplot(plt)

def equalize_histogram(image):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)
    
    equalized_array = cv2.equalizeHist(grayscale_array)
    equalized_image = Image.fromarray(equalized_array)
    
    return equalized_image

def apply_clahe(image, clip_limit=2.0, tile_grid_size=(8, 8)):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)
    
    clahe = cv2.createCLAHE(clipLimit=clip_limit, tileGridSize=tile_grid_size)
    clahe_array = clahe.apply(grayscale_array)
    clahe_image = Image.fromarray(clahe_array)
    
    return clahe_image

def apply_averaging_filter(image, kernel_size=3):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    blurred_image = cv2.blur(image_array, (kernel_size, kernel_size))
    return Image.fromarray(blurred_image)

def apply_gaussian_filter(image, kernel_size=3, sigma=1):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    blurred_image = cv2.GaussianBlur(image_array, (kernel_size, kernel_size), sigmaX=sigma, sigmaY=sigma)
    return Image.fromarray(blurred_image)

def apply_median_filter(image, kernel_size=3):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    median_filtered_image = cv2.medianBlur(image_array, kernel_size)
    return Image.fromarray(median_filtered_image)

def apply_minimum_filter(image, kernel_size=3):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    min_filtered_image = cv2.erode(image_array, kernel)
    return Image.fromarray(min_filtered_image)

def apply_maximum_filter(image, kernel_size=3):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    max_filtered_image = cv2.dilate(image_array, kernel)
    return Image.fromarray(max_filtered_image)

def apply_sobel_filter(image, axis='x', kernel_size=3):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)
    grayscale_array = np.uint8(grayscale_array)
    
    if axis == 'x':
        sobel = cv2.Sobel(grayscale_array, cv2.CV_64F, 1, 0, ksize=kernel_size)
    elif axis == 'y':
        sobel = cv2.Sobel(grayscale_array, cv2.CV_64F, 0, 1, ksize=kernel_size)
    else:
        sobel_x = cv2.Sobel(grayscale_array, cv2.CV_64F, 1, 0, ksize=kernel_size)
        sobel_y = cv2.Sobel(grayscale_array, cv2.CV_64F, 0, 1, ksize=kernel_size)
        sobel = np.sqrt(sobel_x**2 + sobel_y**2)
    
    sobel = np.uint8(np.absolute(sobel))
    sobel_image = Image.fromarray(sobel)
    
    return sobel_image

def apply_laplacian_filter(image, kernel_size=3):
    grayscale_image = image.convert('L')
    grayscale_array = np.array(grayscale_image)
    grayscale_array = np.uint8(grayscale_array)
    
    laplacian = cv2.Laplacian(grayscale_array, cv2.CV_64F, ksize=kernel_size)
    laplacian = np.uint8(np.absolute(laplacian))
    laplacian_image = Image.fromarray(laplacian)
    
    return laplacian_image

def apply_unsharp_mask(image, sigma=1.0, strength=1.5):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    blurred = cv2.GaussianBlur(image_array, (0, 0), sigma)
    unsharp_image = cv2.addWeighted(image_array, 1 + strength, blurred, -strength, 0)
    return Image.fromarray(unsharp_image)

def apply_high_boost(image, sigma=1.0, strength=1.5, k=1.0):
    image_array = np.array(image)
    image_array = np.uint8(image_array)
    blurred = cv2.GaussianBlur(image_array, (0, 0), sigma)
    high_boost_image = cv2.addWeighted(image_array, k + strength, blurred, -strength, 0)
    return Image.fromarray(high_boost_image)

def options(image, key_prefix):
    st.text("Options")

    if st.checkbox("Wykres zmian poziomu szarości", key=f"{key_prefix}_grayscale_levels"):
        line_coord = st.number_input("Podaj nr lini", min_value=0, step=1, value=0, key=f"{key_prefix}_line_coord")
        orientation = st.radio("Wybierz orientacje", ('horizontal', 'vertical'), key=f"{key_prefix}_orientation")
        if st.button("Plot", key=f"{key_prefix}_plot"):
            plot_grayscale_levels(image, line_coord, orientation)

    if st.checkbox("Wybór podobrazu", key=f"{key_prefix}_crop"):
        st.write("Zaznacz obszar")
        cropped_image = st_cropper(image, realtime_update=True, box_color='blue', aspect_ratio=None, key=f"{key_prefix}_cropper")
        st.write("Podgląd")
        st.image(cropped_image, use_column_width=True)
        output_filename = st.text_input("Podaj nazwę pliku: ", "cropped_image.jpg", key=f"{key_prefix}_output_filename")
        if st.button("Zapisz podobraz", key=f"{key_prefix}_save_crop"):
            cropped_image.save(output_filename)
            st.success(f"Zapisano jako: {output_filename}")

def main():
    st.title("Przetwarzanie obrazów w dziedzinie przestrzennej")

    uploaded_file = st.file_uploader("Wybierz obraz...", type=["jpg", "jpeg", "png", "tif"])

    if uploaded_file is not None:
        image = load_image(uploaded_file)
        
        st.header("Orginalny obraz")
        display_image(image, "Orginalny obraz")
        plot_histogram(image, "Histogram orginalnego obrazu")
        options(image, "original")

        st.header("Wyrównanie histogramu")
        equalized_image = equalize_histogram(image)
        display_image(equalized_image, "Obraz po wyrównaniu histogramu")
        plot_histogram(equalized_image, "Histogram po wyrównaniu")
        options(equalized_image, "equalized")

        st.header("Poprawa jakości oparta na lokalnych statystykach")
        clip_limit = st.slider("Clip Limit (Ten parametr ustawia próg ograniczenia kontrastu)", min_value=0.1, max_value=100.0, step=0.1, value=2.0)
        tile_grid_size = st.slider("Tile Grid Size", min_value=1, max_value=16, step=1, value=8)
        clahe_image = apply_clahe(image, clip_limit, (tile_grid_size, tile_grid_size))
        display_image(clahe_image, "CLAHE Image")
        plot_histogram(clahe_image, "Histogram of CLAHE Image")
        options(clahe_image, "clahe")

        st.header("Przekształcenia punktowe")
        st.subheader("T(r) = c1 * r")
        c = st.number_input("Enter 'c1' value:", min_value=0.0, step=0.1, value=1.0)
        transformed_image = image.point(lambda i: i * c)
        display_image(transformed_image, "Transformed Image T(r) = c1 * r")
        plot_histogram(transformed_image, "Histogram of Transformed Image T(r) = c1 * r")
        options(transformed_image, "transformed1")

        st.subheader("T(r) = c2 * log(1 + r)")
        c2 = st.number_input("Enter 'c2' value:", min_value=0.0, step=1.0, value=10.0)
        transformed_image2 = image.point(lambda i: c2 * math.log(1 + i))
        display_image(transformed_image2, "Transformed Image T(r) = c2 * log(1 + r)")
        plot_histogram(transformed_image2, "Histogram of Transformed Image T(r) = c2 * log(1 + r)")
        options(transformed_image2, "transformed2")

        st.subheader("T(r) = 1 / ((m / r)^e)")
        m = st.number_input("Enter 'm' value:", min_value=0.00001, step=0.01, value=0.45)
        e = st.number_input("Enter 'e' value:", min_value=0.00001, step=0.05, value=4.0)

        def transform_contrast(r, m, e) -> float:
            if r == 0:  # Avoid division by zero
                return 0
            return 1.0 / (m / r)**e

        transformed_image3 = image.point(lambda r: transform_contrast(r, m, e))
        display_image(transformed_image3, "Transformed Image T(r) = 1 / ((m / r)^e)")
        plot_histogram(transformed_image3, "Histogram of Transformed Image T(r) = 1 / ((m / r)^e)")
        options(transformed_image3, "transformed3")

        st.subheader("Korekcja gamma")
        c3 = st.slider("Enter 'c3' value:", min_value=0.0, max_value=8.0, step=0.01, value=1.0)
        gamma = st.slider("Enter 'gamma' value:", min_value=0.0, max_value=3.0, step=0.01, value=1.0)
        transformed_image4 = image.point(lambda i: c3 * i**gamma)
        display_image(transformed_image4, "Gamma Corrected Image")
        plot_histogram(transformed_image4, "Histogram of Gamma Corrected Image")
        options(transformed_image4, "transformed4")

        st.header("Filtracja dolnoprzepustowa")

        st.subheader("Filtr uśredniający")
        kernel_size = st.slider("Rozmiar maski", min_value=3, max_value=15, step=2, value=3)
        avg_filtered_image = apply_averaging_filter(image, kernel_size)
        display_image(avg_filtered_image, "Averaging Filtered Image")

        st.subheader("Filtr medianowy")
        median_filtered_image = apply_median_filter(image, kernel_size)
        display_image(median_filtered_image, "Median Filtered Image")

        st.subheader("Filtr minimum")
        min_filtered_image = apply_minimum_filter(image, kernel_size)
        display_image(min_filtered_image, "Minimum Filtered Image")

        st.subheader("Filtr maksimum")
        max_filtered_image = apply_maximum_filter(image, kernel_size)
        display_image(max_filtered_image, "Maximum Filtered Image")

        st.subheader("Filtr Gaussa")
        sigma = st.slider("sigma: ", min_value=1, max_value=200, step=1, value=1)
        gauss_filtered_image = apply_gaussian_filter(image, kernel_size, sigma)
        display_image(gauss_filtered_image, "Gaussian Filtered Image")

        st.header("Filtracja górnoprzepustowa")

        st.subheader("Wykrywanie krawędzi za pomocą filtra Sobela")
        sobel_axis = st.radio("Wybierz oś:", ('x', 'y', 'both'))
        sobel_filtered_image = apply_sobel_filter(image, axis=sobel_axis, kernel_size=kernel_size)
        display_image(sobel_filtered_image, f"Sobel Filtered Image (axis={sobel_axis})")

        st.subheader("Wyostrzanie szczegółów za pomocą Laplasjanu")
        laplacian_filtered_image = apply_laplacian_filter(image, kernel_size)
        display_image(laplacian_filtered_image, "Laplacian Filtered Image")

        st.subheader("Unsharp Masking")
        unsharp_sigma = st.slider("Unsharp Mask Sigma:", min_value=0.1, max_value=10.0, step=0.1, value=1.0)
        unsharp_strength = st.slider("Unsharp Mask Strength:", min_value=0.1, max_value=5.0, step=0.1, value=1.5)
        unsharp_image = apply_unsharp_mask(image, sigma=unsharp_sigma, strength=unsharp_strength)
        display_image(unsharp_image, "Unsharp Masked Image")

        st.subheader("High Boost Filtering")
        high_boost_sigma = st.slider("High Boost Sigma:", min_value=0.1, max_value=10.0, step=0.1, value=1.0)
        high_boost_strength = st.slider("High Boost Strength:", min_value=0.1, max_value=5.0, step=0.1, value=1.5)
        high_boost_k = st.slider("High Boost k Value:", min_value=0.1, max_value=5.0, step=0.1, value=1.0)
        high_boost_image = apply_high_boost(image, sigma=high_boost_sigma, strength=high_boost_strength, k=high_boost_k)
        display_image(high_boost_image, "High Boost Filtered Image")

if __name__ == "__main__":
    main()
