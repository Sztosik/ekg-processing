import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt, freqz

# 1. Wczytanie sygnału EKG
df = pd.read_csv('signals/ekg_noise.csv')
time = df['time'].values
ekg_signal = df['s1'].values
n = len(ekg_signal)

# Parametry
fs = 360.0  # Częstotliwość próbkowania [Hz]

# 2. transformata Fouriera
fft_ekg = np.fft.fft(ekg_signal)
frequencies = np.fft.fftfreq(n, 1/fs)
magnitude = np.abs(fft_ekg) / n
magnitude = magnitude[:n//2] * 2  # Skalowanie amplitudy
frequencies = frequencies[:n//2]

# 3. Low-pass filter design (Butterworth)
def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y

def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y

cutoff_low = 60.0
order = 5
filtered_signal_low = butter_lowpass_filter(ekg_signal, cutoff_low, fs, order)

# FFT of the low-pass filtered signal
fft_filtered_low = np.fft.fft(filtered_signal_low)
magnitude_filtered_low = np.abs(fft_filtered_low) / n
magnitude_filtered_low = magnitude_filtered_low[:n//2] * 2  # Skalowanie amplitudy

# 4. High-pass filter design and application
cutoff_high = 5.0
filtered_signal_band = butter_highpass_filter(filtered_signal_low, cutoff_high, fs, order)

# FFT of the band-pass filtered signal
fft_filtered_band = np.fft.fft(filtered_signal_band)
magnitude_filtered_band = np.abs(fft_filtered_band) / n
magnitude_filtered_band = magnitude_filtered_band[:n//2] * 2  # Skalowanie amplitudy

# Plot the results
fig, axs = plt.subplots(3, 3, figsize=(18, 14))

# Original EKG signal
axs[0, 0].plot(time, ekg_signal)
axs[0, 0].set_title('Oryginalny sygnał EKG')
axs[0, 0].set_xlabel('Czas [s]')
axs[0, 0].set_ylabel('Amplituda')

# Amplitude spectrum of the original EKG signal
axs[0, 1].plot(frequencies, magnitude)
axs[0, 1].set_title('Widmo amplitudowe sygnału EKG')
axs[0, 1].set_xlabel('Częstotliwość [Hz]')
axs[0, 1].set_ylabel('Amplituda')

# Filter characteristics - Low-pass
b, a = butter_lowpass(cutoff_low, fs, order=order)
w, h = freqz(b, a, worN=8000)
axs[0, 2].plot(0.5*fs*w/np.pi, np.abs(h), 'b')
axs[0, 2].set_title('Charakterystyka filtra dolnoprzepustowego')
axs[0, 2].set_xlabel('Częstotliwość [Hz]')
axs[0, 2].set_ylabel('Wzmocnienie')

# Low-pass filtered signal
axs[1, 0].plot(time, filtered_signal_low)
axs[1, 0].set_title('Sygnał EKG po filtracji dolnoprzepustowej')
axs[1, 0].set_xlabel('Czas [s]')
axs[1, 0].set_ylabel('Amplituda')

# Amplitude spectrum of the low-pass filtered signal
axs[1, 1].plot(frequencies, magnitude_filtered_low)
axs[1, 1].set_title('Widmo amplitudowe sygnału po filtracji dolnoprzepustowej')
axs[1, 1].set_xlabel('Częstotliwość [Hz]')
axs[1, 1].set_ylabel('Amplituda')

# Filter characteristics - High-pass
b, a = butter_highpass(cutoff_high, fs, order=order)
w, h = freqz(b, a, worN=8000)
axs[1, 2].plot(0.5*fs*w/np.pi, np.abs(h), 'b')
axs[1, 2].set_title('Charakterystyka filtra górnoprzepustowego')
axs[1, 2].set_xlabel('Częstotliwość [Hz]')
axs[1, 2].set_ylabel('Wzmocnienie')

# Band-pass filtered signal
axs[2, 0].plot(time, filtered_signal_band)
axs[2, 0].set_title('Sygnał EKG po filtracji pasmowoprzepustowej')
axs[2, 0].set_xlabel('Czas [s]')
axs[2, 0].set_ylabel('Amplituda')

# Amplitude spectrum of the band-pass filtered signal
axs[2, 1].plot(frequencies, magnitude_filtered_band)
axs[2, 1].set_title('Widmo amplitudowe sygnału po filtracji pasmowoprzepustowej')
axs[2, 1].set_xlabel('Częstotliwość [Hz]')
axs[2, 1].set_ylabel('Amplituda')

# Difference between original and filtered signal
difference = ekg_signal - filtered_signal_band
axs[2, 2].plot(time, ekg_signal, label='Oryginalny sygnał EKG')
axs[2, 2].plot(time, filtered_signal_band, linestyle='--', label='Sygnał po filtracji')
axs[2, 2].set_title('Porównanie sygnału oryginalnego i po filtracji')
axs[2, 2].set_xlabel('Czas [s]')
axs[2, 2].set_ylabel('Amplituda')
axs[2, 2].legend()

plt.tight_layout()
plt.show()
