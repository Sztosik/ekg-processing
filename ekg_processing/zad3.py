import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 1. Wczytanie sygnału EKG
df = pd.read_csv('signals/ekg100.csv')
ekg_signal = df['s1'].values
n = len(ekg_signal)

# Parametry
fs = 360.0  # Częstotliwość próbkowania [Hz]
t = np.arange(n) / fs  # Oś czasu

# 2. Dyskretna transformata Fouriera
fft_ekg = np.fft.fft(ekg_signal)
frequencies = np.fft.fftfreq(n, 1/fs)
magnitude = np.abs(fft_ekg) / n
magnitude = magnitude[:n//2] * 2  # Skalowanie amplitudy
frequencies = frequencies[:n//2]

# 3. Odwrotna dyskretna transformata Fouriera
ifft_ekg = np.fft.ifft(fft_ekg)

# 4. Różnica między sygnałem oryginalnym a odwrotną transformacją
difference = ekg_signal - ifft_ekg.real

# Wykresy
fig, axs = plt.subplots(2, 3, figsize=(18, 10))

# Wykres sygnału EKG
axs[0, 0].plot(t, ekg_signal, label='Oryginalny sygnał EKG')
axs[0, 0].set_title('Oryginalny sygnał EKG')
axs[0, 0].set_xlabel('Czas [s]')
axs[0, 0].set_ylabel('Amplituda')
axs[0, 0].legend()

# Wykres widma amplitudowego
axs[0, 1].plot(frequencies, magnitude, label='Widmo amplitudowe EKG')
axs[0, 1].set_title('Widmo amplitudowe sygnału EKG')
axs[0, 1].set_xlabel('Częstotliwość [Hz]')
axs[0, 1].set_ylabel('Amplituda')
axs[0, 1].legend()

# Wykres odwrotnej transformaty Fouriera
axs[0, 2].plot(t, ifft_ekg.real, label='Sygnał po IFFT')
axs[0, 2].set_title('Odwrotna transformata Fouriera sygnału EKG')
axs[0, 2].set_xlabel('Czas [s]')
axs[0, 2].set_ylabel('Amplituda')
axs[0, 2].legend()

# Wykres różnicy między sygnałem oryginalnym a odwrotną transformacją
axs[1, 0].plot(t, difference, label='Różnica sygnałów')
axs[1, 0].set_title('Różnica między sygnałem oryginalnym a IFFT')
axs[1, 0].set_xlabel('Czas [s]')
axs[1, 0].set_ylabel('Amplituda')
axs[1, 0].legend()

# Wykres nałożonego oryginalnego sygnału oraz odwrotnej transformacji
axs[1, 1].plot(t, ekg_signal, label='Oryginalny sygnał EKG')
axs[1, 1].plot(t, ifft_ekg.real, label='Sygnał po IFFT')
axs[1, 1].set_title('Nałożony sygnał oryginalny i po IFFT')
axs[1, 1].set_xlabel('Czas [s]')
axs[1, 1].set_ylabel('Amplituda')
axs[1, 1].legend()

# Ukrycie niepotrzebnego wykresu
axs[1, 2].axis('off')

plt.tight_layout()
plt.show()
