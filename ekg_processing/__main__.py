# import numpy as np
# import pandas as pd
# import streamlit as st

# DATA_FREQUENCY_HZ = 100.0

# # Default sinus
# SIN_WAVE_FREQ_HZ = 50
# SAMPLING_FREQ_HZ = 500.0
# TOTAL_SAMPLES = 1000


# # Load signal
# df = pd.read_csv("./signals/ekg1.csv")
# samples_count = len(df)
# df.insert(0, "time", [t / DATA_FREQUENCY_HZ for t in range(1, samples_count + 1)])


# st.title("EKG Processing")
# st.subheader("ekg1.csv")

# # Signals selection
# options = st.multiselect("Sellect signals", [col for col in df.columns][1:], ["s1"])

# # Displaying line chart
# st.line_chart(df, x="time", y=options)

# # Displaying data table
# # st.caption("Table 1: `ekg1.csv` raw data")
# # st.dataframe(df)

# # ===== GENERATING SIN =====
# st.header("Sine wave generator")
# st.subheader("Sine wave")

# # generate signal
# col1, col2, col3 = st.columns(3)
# f0 = col1.number_input("Insert wave frequency [Hz]", value=SIN_WAVE_FREQ_HZ)
# Fs = col2.number_input("Insert sampling frequency [Hz]", value=SAMPLING_FREQ_HZ)
# n = col3.number_input(
#     "Insert total number of samples", value=TOTAL_SAMPLES, format="%d"
# )

# t_step = 1 / Fs  # sample time interval
# f_step = Fs / n  # freq interval

# t = np.linspace(0, (n - 1) * t_step, n)  # time steps
# f = np.linspace(0, (n - 1) * f_step, n)  # freq steps

# y1 = 1 * np.sin(2 * np.pi * f0 * t + 0)  # sinus
# y2 = 1 * np.sin(2 * np.pi * 60.0 * t + 0)  # sinus

# sin1 = {"name": "sin1", "val": y1}
# sin2 = {"name": "sin2", "val": y2}
# sin3 = {"name": "sin1 + sin2", "val": y1 + y2}

# y = st.selectbox(
#     "Select signals", options=(sin1, sin2, sin3), format_func=lambda x: x["name"]
# )["val"]

# fft_values = np.fft.fft(y)
# mag = np.abs(fft_values) / n

# mag_plot = 2 * (np.abs(fft_values) / n)[0 : int(n / 2 + 1)]
# mag_plot[0] = mag_plot[0] / 2
# f_plot = f[0 : int(n / 2 + 1)]

# # data frames
# sin = pd.DataFrame({"y": y, "time": t})
# fft_df = pd.DataFrame({"Frequency": f_plot, "Amplitude": mag_plot})

# # Display Results
# st.line_chart(sin, x="time", y="y")
# st.subheader("Sine wave FFT")
# st.line_chart(fft_df, x="Frequency", y="Amplitude")
