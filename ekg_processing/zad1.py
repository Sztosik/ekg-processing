import matplotlib.pyplot as plt
import pandas as pd

EKG100_FREQUENCY_HZ = 360.0
DEFAULT_FREQUENCY_HZ = 100.0


def plot_signal_with_time(
    signal_name: str, signal_frequency: float = DEFAULT_FREQUENCY_HZ
) -> None:
    """Plot selected signal with time as x-axis.

    Args:
        signal_name: same of signal to plot.
    """
    time_values = df.index / signal_frequency
    plt.plot(time_values, df[signal_name])
    plt.title(f"Plot of {signal_name}")
    plt.xlabel("Time (seconds)")
    plt.ylabel("Value")
    plt.grid(True)
    plt.show()


def time_to_index(time: float, signal_frequency: float = DEFAULT_FREQUENCY_HZ) -> int:
    """Convert time to sample index.

    Args:
        time: time in seconds.
    """
    index = int(time * signal_frequency)
    return index


def save_signal_part(
    signal_name: str,
    start_time: float,
    end_time: float,
    new_file_name: str,
    signal_frequency: float = DEFAULT_FREQUENCY_HZ,
) -> None:
    """Save selected part of signal as a new CSV file.

    Args:
        signal_name: same of signal to plot.
        start_time: start time in seconds.
        end_time: end time in seconds.
        new_file_name: new file name.
    """
    start_index = time_to_index(start_time)
    end_index = time_to_index(end_time)
    selected_signal = df.loc[start_index:end_index, signal_name]
    selected_time = df.index[start_index:end_index] / signal_frequency
    selected_df = pd.DataFrame(
        {"Time (seconds)": selected_time, signal_name: selected_signal}
    )
    selected_df.to_csv(new_file_name + ".csv", index=False)
    print(f"Selected part of signal '{signal_name}' saved as '{new_file_name}'.")


if __name__ == "__main__":
    source_file_name = input("Enter the source file name: (default=ekg1.csv):  ")
    signal_frequency = input("Enter the signal frequency: (default=100 Hz):  ")

    df = pd.read_csv(
        "./signals/" + source_file_name if source_file_name else "./signals/ekg1.csv"
    )

    print("Available signals:")
    print(df.columns)
    signal_name = input("Enter the name of the signal you want to plot: ")

    if signal_name in df.columns:
        plot_signal_with_time(signal_name)

        # Ask for saving a part of the signal
        save_part = input(
            "Do you want to save a part of this signal? (yes/no): "
        ).lower()
        if save_part == "yes":
            start_time = float(
                input("Enter the start time of the part you want to save: ")
            )
            end_time = float(input("Enter the end time of the part you want to save: "))
            new_file_name = input("Enter the name for the new CSV file: ")
            save_signal_part(signal_name, start_time, end_time, new_file_name)
    else:
        print("Invalid signal name. Please try again.")
